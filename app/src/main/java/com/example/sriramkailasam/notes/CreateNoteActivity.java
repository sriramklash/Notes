package com.example.sriramkailasam.notes;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class CreateNoteActivity extends AppCompatActivity {
    private Note note = new Note();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Context context = this;
        setContentView(R.layout.activity_create_note);

        Button saveButton = (Button) this.findViewById(R.id.saveButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveNote(context);
            }
        });
    }

    private void setNoteTitle() {
        EditText editTitle = (EditText) findViewById(R.id.editTitle);
        String title = editTitle.getText().toString();
        note.setTitle(title);
    }

    private void setNoteBody() {
        EditText editBody = (EditText) findViewById(R.id.editBody);
        String body = editBody.getText().toString();
        note.setBody(body);
    }

    private String getNoteTitle() {
        return note.getTitle();
    }

    private String getNoteBody() {
        return note.getBody();
    }

    private int getNotePriority() {
        return note.getPriority();
    }

    void saveNote(Context context) {
        Intent dismissIntent = new Intent(context, MainActivity.class);
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        setNoteTitle();
        setNoteBody();

        ContentValues contentValues = new ContentValues();

        contentValues.put(DatabaseContract.NotesEntry.COLUMN_NAME_TITLE, getNoteTitle());
        contentValues.put(DatabaseContract.NotesEntry.COLUMN_NAME_BODY, getNoteBody());
        contentValues.put(DatabaseContract.NotesEntry.COLUMN_NAME_PRIORITY, getNotePriority());

        db.insert(DatabaseContract.NotesEntry.TABLE_NAME, null, contentValues);
        db.close();
        startActivity(dismissIntent);
    }
}
