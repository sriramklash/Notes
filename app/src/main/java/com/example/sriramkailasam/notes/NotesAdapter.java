package com.example.sriramkailasam.notes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class NotesAdapter extends ArrayAdapter<Note> {
    private int resourceId;
    private List<Note> notes;

    public NotesAdapter(Context context,
                        int resource,
                        int textViewResourceId,
                        List<Note> notesList) {
        super(context, resource,textViewResourceId, notesList);
        resourceId = resource;
        notes = notesList;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        System.out.println("getCount called: " + notes.size());
        return notes.size();
    }

    @Override
    public Note getItem(int position) {
        return notes.get(position);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        System.out.println("getView called");
        LayoutInflater inflater = LayoutInflater.from(getContext());
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceId, parent, false);
            holder = new ViewHolder();
            holder.titleTextView = (TextView) convertView.findViewById(R.id.note_title);
            holder.bodyTextView = (TextView) convertView.findViewById(R.id.note_body);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.titleTextView.setText((getItem(position)).getTitle());
        holder.bodyTextView.setText((getItem(position)).getBody());

        return convertView;
    }

    private class ViewHolder {
        private TextView titleTextView;
        private TextView bodyTextView;
    }
}
