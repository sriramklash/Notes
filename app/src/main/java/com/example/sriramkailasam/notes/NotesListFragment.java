package com.example.sriramkailasam.notes;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import java.util.ArrayList;

public class NotesListFragment extends ListFragment {
    private Context context;
    private ArrayList<Note> notesList = new ArrayList<>();
    private ListView notesListView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        System.out.println("notes list fragment attached");
    }


    @Override
    public View onCreateView(
             LayoutInflater inflater,
             ViewGroup container,
             Bundle savedInstanceState) {
        System.out.println("fragment going to inflate layout");
        View rootView = inflater.inflate(R.layout.fragment_notes_list,
                container, false);
        notesListView = (ListView) rootView.findViewById(android.R.id.list);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        System.out.println("fragment view created");
        NotesAdapter adapter = new NotesAdapter(context,
                        R.layout.notes_list_item,
                        R.id.list_no_items,
                        getAllNotes());
        notesListView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    /*private ArrayList<Note> getAllNotes() {
        openAndQueryDatabase();
        return notesList;
    }*/

    private ArrayList<Note> getAllNotes() {
        DatabaseHelper helper = new DatabaseHelper(context);
        SQLiteDatabase database = helper.getReadableDatabase();

        String selectQuery =
                "SELECT title, body FROM " + DatabaseContract.NotesEntry.TABLE_NAME;

        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor != null) {
            cursor.moveToFirst();
            do {
                Note note = new Note();
                note.setTitle(cursor.getString(cursor.getColumnIndex("title")));
                note.setBody(cursor.getString(cursor.getColumnIndex("body")));

                notesList.add(note);
            } while (cursor.moveToNext());

            cursor.close();
        }
        else {
            System.out.println("cursor is null");
        }
        return notesList;
    }

    /*private void openAndQueryDatabase() {
        DatabaseHelper helper = new DatabaseHelper(context);
        SQLiteDatabase database = helper.getReadableDatabase();

        String selectQuery =
                "SELECT title, body FROM " + DatabaseContract.NotesEntry.TABLE_NAME;

        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor != null) {
            cursor.moveToFirst();
            do {
                Note note = new Note();
                note.setTitle(cursor.getString(cursor.getColumnIndex("title")));
                note.setBody(cursor.getString(cursor.getColumnIndex("body")));

                notesList.add(note);
            } while (cursor.moveToNext());

            cursor.close();
        }
        else {
            System.out.println("cursor is null");
        }
    }*/
}
