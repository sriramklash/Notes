package com.example.sriramkailasam.notes;

public class Note {
    private String title;
    private String body;
    private boolean starred = false;

    Note() {
        this.title = "Untitled Note";
        this.body = "";
    }

    public void setTitle(String title) {
        if (title.equals(""))
            this.title = "Untitled Note";
        else
            this.title = title;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setStarred(boolean starred) {
        this.starred = starred;
    }

    public String getTitle() {
        return this.title;
    }

    public String getBody() {
        return this.body;
    }

    public boolean isStarred() {
        return this.starred;
    }
}
