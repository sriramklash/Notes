package com.example.sriramkailasam.notes;

import android.provider.BaseColumns;

public final class DatabaseContract  {
    private DatabaseContract() {}

    public static final class NotesEntry implements BaseColumns {
        public static final String TABLE_NAME           = "notes";
        public static final String COLUMN_NAME_TITLE    = "title";
        public static final String COLUMN_NAME_BODY     = "body";
        public static final String COLUMN_NAME_PRIORITY = "priority";
    }
}
