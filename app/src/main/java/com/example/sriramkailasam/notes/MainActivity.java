package com.example.sriramkailasam.notes;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ArrayList<Note> notesList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        createListView(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createNote();
            }
        });
    }

    private void createNote() {
        Intent createNoteIntent = new Intent(this, CreateNoteActivity.class);
        startActivity(createNoteIntent);
    }

    private void createListView(Context context) {
        ListView notesListView =
                (ListView) ((Activity) context).findViewById(android.R.id.list);
        NotesAdapter adapter = new NotesAdapter(context,
                                                R.layout.notes_list_item,
                                                R.id.list_no_items,
                                                getAllNotes(context));

        notesListView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private ArrayList<Note> getAllNotes(Context context) {
        DatabaseHelper helper = new DatabaseHelper(context);
        SQLiteDatabase database = helper.getReadableDatabase();

        String selectQuery =
                "SELECT title, body FROM " + DatabaseContract.NotesEntry.TABLE_NAME;
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor != null) {
            cursor.moveToFirst();
            do {
                Note note = new Note();
                note.setTitle(cursor.getString(cursor.getColumnIndex("title")));
                note.setBody(cursor.getString(cursor.getColumnIndex("body")));

                notesList.add(note);
            } while (cursor.moveToNext());

            cursor.close();
        }
        else {
            System.out.println("cursor is null");
        }
        return notesList;
    }
}
