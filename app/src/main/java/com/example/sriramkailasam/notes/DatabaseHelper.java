package com.example.sriramkailasam.notes;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Notes.db";

    private static final String SQL_CREATE_ENTRIES =
        "CREATE TABLE " + DatabaseContract.NotesEntry.TABLE_NAME +
        " ( " + DatabaseContract.NotesEntry._ID + " INTEGER PRIMARY KEY, " +
        DatabaseContract.NotesEntry.COLUMN_NAME_TITLE + " CHAR NOT NULL, " +
        DatabaseContract.NotesEntry.COLUMN_NAME_BODY + " TEXT, " +
        DatabaseContract.NotesEntry.COLUMN_NAME_PRIORITY + " INT);";

    private static final String SQL_DELETE_ENTRIES =
        "DROP TABLE IF EXISTS " + DatabaseContract.NotesEntry.TABLE_NAME;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);

        onCreate(db);
    }
}
